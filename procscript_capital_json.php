<?php

//ONLY USED FOR DEV, IGNORE OTHERWISE

//echo "OK";
//error_reporting(-1);
$full_json_data = file_get_contents('capitals.geojson.txt');
//print_r($full_json_data);
$full_json_php = json_decode($full_json_data);

$features_array = $full_json_php->features;
//print_r($features_array);
$arr = array();
$arr['capitals'] = array();
foreach($features_array as $feature) {
	$arr['capitals'][] = array(
		'country' => $feature->properties->country,
		'city' => $feature->properties->city,
		'lat' => $feature->geometry->coordinates[1],
		'long' => $feature->geometry->coordinates[0]
	);
}

$arr_json = json_encode($arr);
print_r($arr_json);

?>