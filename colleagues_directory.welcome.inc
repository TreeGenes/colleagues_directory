<?php

/**
 * Forwards to edit profile link
 *
 *
 * @return null
 *   Returns null but does a redirect
 *
 */
function colleagues_directory_generate_welcome_editprofile_page() {
	global $base_url;
	global $site_name;
	global $user;
	if($user->uid > 0) {
		$contact_entity_id = tripal_contact_profile_get_contact_entity_id();
		if ($contact_entity_id <= 0) {
			drupal_goto("$base_url/bio_data/add/" . tripal_contact_profile_getMachineName_id());
		}
		else {
			drupal_goto("$base_url/bio_data/$contact_entity_id/edit");
		}		
	}
	else {
		drupal_goto("$base_url/user/login");
	}
}

/**
 * Forwards to change page URL
 *
 *
 * @return null
 *   Returns null but forwards to change password url once logged in
 *
 */
function colleagues_directory_generate_welcome_changepassword_page() {
	global $base_url;
	global $site_name;
	global $user;
	if($user->uid > 0) {
			drupal_goto("$base_url/user/" . $user->uid . "/edit");	
	}
	else {
		drupal_goto("$base_url/user/login");
	}
}

/**
 * Generates welcome page
 *
 *
 * @return string
 *   The welcome page HTML output
 *
 */
function colleagues_directory_generate_welcome_page() {
	global $base_url;
	global $user;
	$uid = $user->uid;
	$site_name = variable_get('site_name', "Default site name");
	
	$items = array();
	
	//Edit Profile items
	$items[] = array(
		'title' => variable_get('colleagues_directory_welcome_editprofile_link_text', 'Add/Edit Profile'),
		'link' => colleagues_directory_welcome_tokenizer($base_url . variable_get('colleagues_directory_welcome_editprofile_link_url')),
		'image' => variable_get('colleagues_directory_welcome_editprofile_link_image'),
	);
	
	//Change password items
	$items[] = array(
		'title' => variable_get('colleagues_directory_welcome_changepassword_link_text', 'Change Password'),
		'link' => colleagues_directory_welcome_tokenizer($base_url . variable_get('colleagues_directory_welcome_changepassword_link_url')),
		'image' => variable_get('colleagues_directory_welcome_changepassword_link_image'),
	);	
	
	//Get all the rest of custom items
	$item_count = variable_get('colleagues_directory_welcome_items_count', 0);
	for($k=0; $k<$item_count; $k++) {
		$i = $k + 1;
		$items[] = array(
			'title' => variable_get('colleagues_directory_welcome_link_text_' . $i),
			'link' => colleagues_directory_welcome_tokenizer(variable_get('colleagues_directory_welcome_link_url_' . $i)),
			'image' => variable_get('colleagues_directory_welcome_link_image_' . $i),
		);
	}
	
	if($uid > 0) {
		$markup = "";
		$markup .= "
			<style>
			#welcome_table td{
			   width: 32%;
			}

			#welcome_table .card {
				 height: 260px;
				 padding-top: 20px;
			}
			</style>
			<table id='welcome_table'>
			<thead>
			<div class='card'>
			<p style='font-size:16px; padding-top: 10px; padding-bottom: 10px !important;'>Welcome to $site_name</p>
			<!-- <img src='$base_url/sites/default/files/uploads/logo_mod2.png' /> -->
			<p style='padding-top: 10px; padding-bottom: 10px !important;'>You're now logged in and ready to go!</p>
			</div>
			</thead>
		";
		
		$item_count = count($items);
		$count = 0;
		$rows = ceil($item_count / 3);
		for($i=0; $i<$rows; $i++) {
			//row i
			$markup .= "<tr>";
			$k = $count;
			for($j = $k; $j < $k + 3;  $j++) {
				$item = @$items[$count];
				if(isset($item)){
					$item_title = $item['title'];
					$item_link = $item['link'];
					$item_image = file_create_url(file_load($item['image'])->uri);
					//print_r($item_image);
					$markup .= "
					<td>
						<div class='card'>
							<img width='175px' src='$item_image' />
							<h4><a href='$item_link'>$item_title</a></h4>
						</div>
					</td>";
				}
				$count = $count + 1;
			}
			$markup .= "</tr>";
		}
		
		$markup .= "</table>";

	}
	else {
		header("Location: $base_url/user/login");
		$markup .= "<a href='$base_url/user/login'>You must be logged in to view this page</a>";
	}


	$content['raw_markup'] = array(
		'#type' => 'markup',
		'#markup' => $markup,
	);

	return $content;	
}

/**
 * Custom tokenizer used on welcome page
 *
 * @param string $text
 *   The text to tokenize
 *
 * @return string
 *   The modified text results
 *
 */
function colleagues_directory_welcome_tokenizer($text) {
	global $user;
	$text = str_ireplace('<uid>', $user->uid, $text);
	return $text;
}
?>