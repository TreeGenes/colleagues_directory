[![Tripal Rating Silver Status](https://tripal.readthedocs.io/en/7.x-3.x/_images/Tripal-Silver.png)](https://tripal.readthedocs.io/en/7.x-3.x/extensions/module_rating.html#Silver)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3463337.svg)](https://doi.org/10.5281/zenodo.3463337)
Colleagues Directory depends on Tripal and in particular the Tripal Contact Profile module. 
It generates both a page that looks like a Drupal view (but is not) with built-in search that makes it to display your user's profiles.


# Install Instructions
* Make sure you have the latest version of Tripal (3.x with Chado v1.3) installed
* Make sure you have installed the Tripal Contact Profile module and followed the install instructions
* Copy the files in this repository to your Tripal /sites/all/modules/colleagues_directory folder (if it does not exist, create it the folder)
* Go to Drupal Administration page, visit the Modules section and Enable Colleagues Directory module
* Go to Drupal Administration page, visit Tripal and click on Colleagues Directory Configuration to further fine tune the features available
* Create a menu link in your Drupal/Tripal website that points to colleagues_directory/search

