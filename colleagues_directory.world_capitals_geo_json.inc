<?php
/**
 * @file
 * World Capitals integration for the Colleagues Directory module.
 */


global $world_capitals_geo_json;

$world_capitals_geo_json = '
{
  "capitals": [
    {
      "country": "Bangladesh",
      "city": "Dhaka",
      "lat": 23.43,
      "long": 90.24
    },
    {
      "country": "Belgium",
      "city": "Brussels",
      "lat": 50.5,
      "long": 4.2
    },
    {
      "country": "Burkina Faso",
      "city": "Ouagadougou",
      "lat": 12.22,
      "long": -1.31
    },
    {
      "country": "Bulgaria",
      "city": "Sofia",
      "lat": 42.41,
      "long": 23.19
    },
    {
      "country": "Bosnia and Herzegovina",
      "city": "Sarajevo",
      "lat": 43.52,
      "long": 18.25
    },
    {
      "country": "Barbados",
      "city": "Bridgetown",
      "lat": 13.06,
      "long": -59.37
    },
    {
      "country": "Wallis and Futuna",
      "city": "Mata-Utu",
      "lat": -13.57,
      "long": -171.56
    },
    {
      "country": "Saint Barthelemy",
      "city": "Gustavia",
      "lat": 17.53,
      "long": -62.51
    },
    {
      "country": "Bermuda",
      "city": "Hamilton",
      "lat": 32.17,
      "long": -64.47
    },
    {
      "country": "Brunei",
      "city": "Bandar Seri Begawan",
      "lat": 4.53,
      "long": 114.56
    },
    {
      "country": "Bolivia",
      "city": "La Paz",
      "lat": -16.3,
      "long": -68.09
    },
    {
      "country": "Bahrain",
      "city": "Manama",
      "lat": 26.14,
      "long": 50.34
    },
    {
      "country": "Burundi",
      "city": "Bujumbura",
      "lat": -3.22,
      "long": 29.21
    },
    {
      "country": "Benin",
      "city": "Porto-Novo",
      "lat": 6.29,
      "long": 2.37
    },
    {
      "country": "Bhutan",
      "city": "Thimphu",
      "lat": 27.29,
      "long": 89.36
    },
    {
      "country": "Jamaica",
      "city": "Kingston",
      "lat": 18,
      "long": -76.48
    },
    {
      "country": "Bouvet Island",
      "city": null,
      "lat": -54.26,
      "long": 3.24
    },
    {
      "country": "Botswana",
      "city": "Gaborone",
      "lat": -24.45,
      "long": 25.55
    },
    {
      "country": "Samoa",
      "city": "Apia",
      "lat": -13.5,
      "long": -171.44
    },
    {
      "country": "Brazil",
      "city": "Brasilia",
      "lat": -15.47,
      "long": -47.55
    },
    {
      "country": "Bahamas",
      "city": "Nassau",
      "lat": 25.05,
      "long": -77.21
    },
    {
      "country": "Jersey",
      "city": "Saint Helier",
      "lat": 49.11,
      "long": -2.06
    },
    {
      "country": "Belarus",
      "city": "Minsk",
      "lat": 53.54,
      "long": 27.34
    },
    {
      "country": "Belize",
      "city": "Belmopan",
      "lat": 17.15,
      "long": -88.46
    },
    {
      "country": "Russia",
      "city": "Moscow",
      "lat": 55.45,
      "long": 37.35
    },
    {
      "country": "Rwanda",
      "city": "Kigali",
      "lat": -1.57,
      "long": 30.04
    },
    {
      "country": "Serbia",
      "city": "Belgrade",
      "lat": 44.5,
      "long": 20.3
    },
    {
      "country": "Timor-Leste",
      "city": "Dili",
      "lat": -8.35,
      "long": 125.36
    },
    {
      "country": "Turkmenistan",
      "city": "Ashgabat",
      "lat": 37.57,
      "long": 58.23
    },
    {
      "country": "Tajikistan",
      "city": "Dushanbe",
      "lat": 38.35,
      "long": 68.48
    },
    {
      "country": "Romania",
      "city": "Bucharest",
      "lat": 44.26,
      "long": 26.06
    },
    {
      "country": "Tokelau",
      "city": null,
      "lat": -9,
      "long": -172
    },
    {
      "country": "Guinea-Bissau",
      "city": "Bissau",
      "lat": 11.51,
      "long": -15.35
    },
    {
      "country": "Guam",
      "city": "Hagatna",
      "lat": 13.28,
      "long": 144.44
    },
    {
      "country": "Guatemala",
      "city": "Guatemala City",
      "lat": 14.37,
      "long": -90.31
    },
    {
      "country": "Greece",
      "city": "Athens",
      "lat": 37.59,
      "long": 23.44
    },
    {
      "country": "Equatorial Guinea",
      "city": "Malabo",
      "lat": 3.45,
      "long": 8.47
    },
    {
      "country": "Japan",
      "city": "Tokyo",
      "lat": 35.41,
      "long": 139.45
    },
    {
      "country": "Guyana",
      "city": "Georgetown",
      "lat": 6.48,
      "long": -58.1
    },
    {
      "country": "Guernsey",
      "city": "Saint Peter Port",
      "lat": 49.27,
      "long": -2.32
    },
    {
      "country": "Georgia",
      "city": "T\'bilisi",
      "lat": 41.43,
      "long": 44.47
    },
    {
      "country": "Grenada",
      "city": "Saint George\'s",
      "lat": 12.03,
      "long": -61.45
    },
    {
      "country": "United Kingdom",
      "city": "London",
      "lat": 51.3,
      "long": -0.1
    },
    {
      "country": "Gabon",
      "city": "Libreville",
      "lat": 0.23,
      "long": 9.27
    },
    {
      "country": "El Salvador",
      "city": "San Salvador",
      "lat": 13.42,
      "long": -89.12
    },
    {
      "country": "Guinea",
      "city": "Conakry",
      "lat": 9.33,
      "long": -13.42
    },
    {
      "country": "Gambia",
      "city": "Banjul",
      "lat": 13.27,
      "long": -16.34
    },
    {
      "country": "Greenland",
      "city": "Nuuk",
      "lat": 64.11,
      "long": -51.45
    },
    {
      "country": "Gibraltar",
      "city": "Gibraltar",
      "lat": 36.08,
      "long": -5.21
    },
    {
      "country": "Ghana",
      "city": "Accra",
      "lat": 5.33,
      "long": -0.13
    },
    {
      "country": "Oman",
      "city": "Muscat",
      "lat": 23.37,
      "long": 58.35
    },
    {
      "country": "Tunisia",
      "city": "Tunis",
      "lat": 36.48,
      "long": 10.11
    },
    {
      "country": "Jordan",
      "city": "Amman",
      "lat": 31.57,
      "long": 35.56
    },
    {
      "country": "Croatia",
      "city": "Zagreb",
      "lat": 45.48,
      "long": 16
    },
    {
      "country": "Haiti",
      "city": "Port-au-Prince",
      "lat": 18.32,
      "long": -72.2
    },
    {
      "country": "Hungary",
      "city": "Budapest",
      "lat": 47.3,
      "long": 19.05
    },
    {
      "country": "Hong Kong",
      "city": "Hong Kong",
      "lat": 22.15,
      "long": 114.1
    },
    {
      "country": "Honduras",
      "city": "Tegucigalpa",
      "lat": 14.06,
      "long": -87.13
    },
    {
      "country": "Heard Island and McDonald Islands",
      "city": null,
      "lat": -53.06,
      "long": 72.31
    },
    {
      "country": "Venezuela",
      "city": "Caracas",
      "lat": 10.3,
      "long": -66.56
    },
    {
      "country": "Puerto Rico",
      "city": "San Juan",
      "lat": 18.28,
      "long": -66.07
    },
    {
      "country": "Palestinian Territory",
      "city": null,
      "lat": 31.25,
      "long": 34.2
    },
    {
      "country": "Palau",
      "city": "Melekeok",
      "lat": 7.29,
      "long": 134.38
    },
    {
      "country": "Portugal",
      "city": "Lisbon",
      "lat": 38.43,
      "long": -9.08
    },
    {
      "country": "Svalbard",
      "city": "Longyearbyen",
      "lat": 78.13,
      "long": 15.33
    },
    {
      "country": "Paraguay",
      "city": "Asuncion",
      "lat": -25.16,
      "long": -57.4
    },
    {
      "country": "Iraq",
      "city": "Baghdad",
      "lat": 33.2,
      "long": 44.23
    },
    {
      "country": "Panama",
      "city": "Panama City",
      "lat": 8.58,
      "long": -79.32
    },
    {
      "country": "French Polynesia",
      "city": "Papeete",
      "lat": -17.32,
      "long": -149.34
    },
    {
      "country": "Papua New Guinea",
      "city": "Port Moresby",
      "lat": -9.3,
      "long": 147.1
    },
    {
      "country": "Peru",
      "city": "Lima",
      "lat": -12.03,
      "long": -77.03
    },
    {
      "country": "Pakistan",
      "city": "Islamabad",
      "lat": 33.42,
      "long": 73.1
    },
    {
      "country": "Philippines",
      "city": "Manila",
      "lat": 14.35,
      "long": 121
    },
    {
      "country": "Pitcairn",
      "city": "Adamstown",
      "lat": -25.04,
      "long": -130.05
    },
    {
      "country": "Poland",
      "city": "Warsaw",
      "lat": 52.15,
      "long": 21
    },
    {
      "country": "Saint Pierre and Miquelon",
      "city": "Saint-Pierre",
      "lat": 46.46,
      "long": -56.11
    },
    {
      "country": "Zambia",
      "city": "Lusaka",
      "lat": -15.25,
      "long": 28.17
    },
    {
      "country": "Western Sahara",
      "city": null,
      "lat": 24.3,
      "long": -13
    },
    {
      "country": "Estonia",
      "city": "Tallinn",
      "lat": 59.26,
      "long": 24.43
    },
    {
      "country": "Egypt",
      "city": "Cairo",
      "lat": 30.03,
      "long": 31.15
    },
    {
      "country": "South Africa",
      "city": "Pretoria",
      "lat": -25.42,
      "long": 28.13
    },
    {
      "country": "Ecuador",
      "city": "Quito",
      "lat": -0.13,
      "long": -78.3
    },
    {
      "country": "Italy",
      "city": "Rome",
      "lat": 41.54,
      "long": 12.29
    },
    {
      "country": "Vietnam",
      "city": "Hanoi",
      "lat": 21.02,
      "long": 105.51
    },
    {
      "country": "Solomon Islands",
      "city": "Honiara",
      "lat": -9.26,
      "long": 159.57
    },
    {
      "country": "European Union",
      "city": null,
      "lat": 50.1021,
      "long": 9.9
    },
    {
      "country": "Ethiopia",
      "city": "Addis Ababa",
      "lat": 9.02,
      "long": 38.42
    },
    {
      "country": "Somalia",
      "city": "Mogadishu",
      "lat": 2.04,
      "long": 45.22
    },
    {
      "country": "Zimbabwe",
      "city": "Harare",
      "lat": -17.5,
      "long": 31.03
    },
    {
      "country": "Saudi Arabia",
      "city": "Riyadh",
      "lat": 24.38,
      "long": 46.43
    },
    {
      "country": "Spain",
      "city": "Madrid",
      "lat": 40.24,
      "long": -3.41
    },
    {
      "country": "Eritrea",
      "city": "Asmara",
      "lat": 15.2,
      "long": 38.56
    },
    {
      "country": "Montenegro",
      "city": "Podgorica",
      "lat": 42.26,
      "long": 19.16
    },
    {
      "country": "Moldova",
      "city": "Chisinau",
      "lat": 47,
      "long": 28.51
    },
    {
      "country": "Madagascar",
      "city": "Antananarivo",
      "lat": -18.55,
      "long": 47.31
    },
    {
      "country": "Saint Martin",
      "city": "Marigot",
      "lat": 18.04,
      "long": -63.05
    },
    {
      "country": "Morocco",
      "city": "Rabat",
      "lat": 34.01,
      "long": -6.49
    },
    {
      "country": "Monaco",
      "city": "Monaco",
      "lat": 43.44,
      "long": 7.25
    },
    {
      "country": "Uzbekistan",
      "city": "Tashkent",
      "lat": 41.2,
      "long": 69.18
    },
    {
      "country": "Myanmar",
      "city": "Rangoon",
      "lat": 16.48,
      "long": 96.09
    },
    {
      "country": "Mali",
      "city": "Bamako",
      "lat": 12.39,
      "long": -8
    },
    {
      "country": "Macao",
      "city": null,
      "lat": 22.1,
      "long": 113.33
    },
    {
      "country": "Mongolia",
      "city": "Ulaanbaatar",
      "lat": 47.55,
      "long": 106.55
    },
    {
      "country": "Marshall Islands",
      "city": "Majuro",
      "lat": 7.06,
      "long": 171.23
    },
    {
      "country": "Macedonia",
      "city": "Skopje",
      "lat": 42,
      "long": 21.26
    },
    {
      "country": "Mauritius",
      "city": "Port Louis",
      "lat": -20.09,
      "long": 57.29
    },
    {
      "country": "Malta",
      "city": "Valletta",
      "lat": 35.53,
      "long": 14.3
    },
    {
      "country": "Malawi",
      "city": "Lilongwe",
      "lat": -13.59,
      "long": 33.47
    },
    {
      "country": "Maldives",
      "city": "Male",
      "lat": 4.1,
      "long": 73.3
    },
    {
      "country": "Northern Mariana Islands",
      "city": "Saipan",
      "lat": 15.12,
      "long": 145.45
    },
    {
      "country": "Montserrat",
      "city": "Plymouth",
      "lat": 16.42,
      "long": -62.13
    },
    {
      "country": "Mauritania",
      "city": "Nouakchott",
      "lat": 18.07,
      "long": -16.02
    },
    {
      "country": "Isle of Man",
      "city": "Douglas",
      "lat": 54.09,
      "long": -4.29
    },
    {
      "country": "Uganda",
      "city": "Kampala",
      "lat": 0.19,
      "long": 32.25
    },
    {
      "country": "Malaysia",
      "city": "Kuala Lumpur",
      "lat": 3.1,
      "long": 101.42
    },
    {
      "country": "Mexico",
      "city": "Mexico City",
      "lat": 19.26,
      "long": -99.08
    },
    {
      "country": "Israel",
      "city": "Jerusalem",
      "lat": 31.46,
      "long": 35.14
    },
    {
      "country": "France",
      "city": "Paris",
      "lat": 48.52,
      "long": 2.2
    },
    {
      "country": "British Indian Ocean Territory",
      "city": null,
      "lat": -6,
      "long": 71.3
    },
    {
      "country": "Saint Helena Ascension and Tristan da Cunha",
      "city": "Jamestown",
      "lat": -15.56,
      "long": -5.44
    },
    {
      "country": "Finland",
      "city": "Helsinki",
      "lat": 60.1,
      "long": 24.56
    },
    {
      "country": "Fiji",
      "city": "Suva",
      "lat": -18.08,
      "long": 178.25
    },
    {
      "country": "Falkland Islands",
      "city": "Stanley",
      "lat": -51.42,
      "long": -57.51
    },
    {
      "country": "Micronesia",
      "city": "Palikir",
      "lat": 6.55,
      "long": 158.09
    },
    {
      "country": "Faroe Islands",
      "city": "Torshavn",
      "lat": 62.01,
      "long": -6.46
    },
    {
      "country": "Nicaragua",
      "city": "Managua",
      "lat": 12.09,
      "long": -86.17
    },
    {
      "country": "Netherlands",
      "city": "Amsterdam",
      "lat": 52.23,
      "long": 4.54
    },
    {
      "country": "Norway",
      "city": "Oslo",
      "lat": 59.55,
      "long": 10.45
    },
    {
      "country": "Namibia",
      "city": "Windhoek",
      "lat": -22.34,
      "long": 17.05
    },
    {
      "country": "Vanuatu",
      "city": "Port-Vila",
      "lat": -17.44,
      "long": 168.19
    },
    {
      "country": "New Caledonia",
      "city": "Noumea",
      "lat": -22.16,
      "long": 166.27
    },
    {
      "country": "Niger",
      "city": "Niamey",
      "lat": 13.31,
      "long": 2.07
    },
    {
      "country": "Norfolk Island",
      "city": "Kingston",
      "lat": -29.03,
      "long": 167.58
    },
    {
      "country": "Nigeria",
      "city": "Abuja",
      "lat": 9.05,
      "long": 7.32
    },
    {
      "country": "New Zealand",
      "city": "Wellington",
      "lat": -41.28,
      "long": 174.51
    },
    {
      "country": "Nepal",
      "city": "Kathmandu",
      "lat": 27.43,
      "long": 85.19
    },
    {
      "country": "Nauru",
      "city": null,
      "lat": -0.32,
      "long": 166.55
    },
    {
      "country": "Niue",
      "city": "Alofi",
      "lat": -19.01,
      "long": -169.55
    },
    {
      "country": "Cook Islands",
      "city": "Avarua",
      "lat": -21.12,
      "long": -159.46
    },
    {
      "country": "Ivory Coast",
      "city": "Yamoussoukro",
      "lat": 6.49,
      "long": -5.17
    },
    {
      "country": "Switzerland",
      "city": "Bern",
      "lat": 46.57,
      "long": 7.26
    },
    {
      "country": "Colombia",
      "city": "Bogota",
      "lat": 4.36,
      "long": -74.05
    },
    {
      "country": "China",
      "city": "Beijing",
      "lat": 39.55,
      "long": 116.23
    },
    {
      "country": "Cameroon",
      "city": "Yaounde",
      "lat": 3.52,
      "long": 11.31
    },
    {
      "country": "Chile",
      "city": "Santiago",
      "lat": -33.27,
      "long": -70.4
    },
    {
      "country": "Cocos Islands",
      "city": "West Island",
      "lat": -12.1,
      "long": 96.5
    },
    {
      "country": "Canada",
      "city": "Ottawa",
      "lat": 45.25,
      "long": -75.42
    },
    {
      "country": "Congo Republic",
      "city": "Brazzaville",
      "lat": -4.15,
      "long": 15.17
    },
    {
      "country": "Central African Republic",
      "city": "Bangui",
      "lat": 4.22,
      "long": 18.35
    },
    {
      "country": "Congo Democratic Republic",
      "city": "Kinshasa",
      "lat": -4.19,
      "long": 15.18
    },
    {
      "country": "Czech Republic",
      "city": "Prague",
      "lat": 50.05,
      "long": 14.28
    },
    {
      "country": "Cyprus",
      "city": "Nicosia",
      "lat": 35.1,
      "long": 33.22
    },
    {
      "country": "Christmas Island",
      "city": "The Settlement",
      "lat": -10.25,
      "long": 105.43
    },
    {
      "country": "Costa Rica",
      "city": "San Jose",
      "lat": 9.56,
      "long": -84.05
    },
    {
      "country": "Cape Verde",
      "city": "Praia",
      "lat": 14.55,
      "long": -23.31
    },
    {
      "country": "Cuba",
      "city": "Havana",
      "lat": 23.07,
      "long": -82.21
    },
    {
      "country": "Swaziland",
      "city": "Mbabane",
      "lat": -26.18,
      "long": 31.06
    },
    {
      "country": "Syria",
      "city": "Damascus",
      "lat": 33.3,
      "long": 36.18
    },
    {
      "country": "Kyrgyzstan",
      "city": "Bishkek",
      "lat": 42.52,
      "long": 74.36
    },
    {
      "country": "Kenya",
      "city": "Nairobi",
      "lat": -1.17,
      "long": 36.49
    },
    {
      "country": "Suriname",
      "city": "Paramaribo",
      "lat": 5.5,
      "long": -55.1
    },
    {
      "country": "Kiribati",
      "city": "Tarawa",
      "lat": 1.19,
      "long": 172.58
    },
    {
      "country": "Cambodia",
      "city": "Phnom Penh",
      "lat": 11.33,
      "long": 104.55
    },
    {
      "country": "Saint Kitts and Nevis",
      "city": "Basseterre",
      "lat": 17.18,
      "long": -62.43
    },
    {
      "country": "Comoros",
      "city": "Moroni",
      "lat": -11.42,
      "long": 43.14
    },
    {
      "country": "Sao Tome and Principe",
      "city": "Sao Tome",
      "lat": 0.12,
      "long": 6.39
    },
    {
      "country": "Slovakia",
      "city": "Bratislava",
      "lat": 48.09,
      "long": 17.07
    },
    {
      "country": "Korea South",
      "city": "Seoul",
      "lat": 37.33,
      "long": 126.59
    },
    {
      "country": "Slovenia",
      "city": "Ljubljana",
      "lat": 46.03,
      "long": 14.31
    },
    {
      "country": "Korea North",
      "city": "Pyongyang",
      "lat": 39.01,
      "long": 125.45
    },
    {
      "country": "Kuwait",
      "city": "Kuwait City",
      "lat": 29.22,
      "long": 47.58
    },
    {
      "country": "Senegal",
      "city": "Dakar",
      "lat": 14.4,
      "long": -17.26
    },
    {
      "country": "San Marino",
      "city": "San Marino",
      "lat": 43.56,
      "long": 12.25
    },
    {
      "country": "Sierra Leone",
      "city": "Freetown",
      "lat": 8.3,
      "long": -13.15
    },
    {
      "country": "Seychelles",
      "city": "Victoria",
      "lat": -4.38,
      "long": 55.27
    },
    {
      "country": "Kazakhstan",
      "city": "Astana",
      "lat": 51.1,
      "long": 71.25
    },
    {
      "country": "Cayman Islands",
      "city": "George Town",
      "lat": 19.18,
      "long": -81.23
    },
    {
      "country": "Singapore",
      "city": "Singapore",
      "lat": 1.17,
      "long": 103.51
    },
    {
      "country": "Sweden",
      "city": "Stockholm",
      "lat": 59.2,
      "long": 18.03
    },
    {
      "country": "Sudan",
      "city": "Khartoum",
      "lat": 15.36,
      "long": 32.32
    },
    {
      "country": "Dominican Republic",
      "city": "Santo Domingo",
      "lat": 18.28,
      "long": -69.54
    },
    {
      "country": "Dominica",
      "city": "Roseau",
      "lat": 15.18,
      "long": -61.24
    },
    {
      "country": "Djibouti",
      "city": "Djibouti",
      "lat": 11.35,
      "long": 43.09
    },
    {
      "country": "Denmark",
      "city": "Copenhagen",
      "lat": 55.4,
      "long": 12.35
    },
    {
      "country": "British Virgin Islands",
      "city": "Road Town",
      "lat": 18.27,
      "long": -64.37
    },
    {
      "country": "Germany",
      "city": "Berlin",
      "lat": 52.31,
      "long": 13.24
    },
    {
      "country": "Yemen",
      "city": "Sanaa",
      "lat": 15.21,
      "long": 44.12
    },
    {
      "country": "Algeria",
      "city": "Algiers",
      "lat": 36.45,
      "long": 3.03
    },
    {
      "country": "United States",
      "city": "Washington, DC",
      "lat": 38.53,
      "long": -77.02
    },
    {
      "country": "Uruguay",
      "city": "Montevideo",
      "lat": -34.53,
      "long": -56.11
    },
    {
      "country": "Mayotte",
      "city": "Mamoudzou",
      "lat": -12.46,
      "long": 45.13
    },
    {
      "country": "Lebanon",
      "city": "Beirut",
      "lat": 33.52,
      "long": 35.3
    },
    {
      "country": "Saint Lucia",
      "city": "Castries",
      "lat": 14.01,
      "long": -61
    },
    {
      "country": "Laos",
      "city": "Vientiane",
      "lat": 17.58,
      "long": 102.36
    },
    {
      "country": "Tuvalu",
      "city": "Funafuti",
      "lat": -8.3,
      "long": 179.12
    },
    {
      "country": "Taiwan",
      "city": "Taipei",
      "lat": 25.03,
      "long": 121.3
    },
    {
      "country": "Trinidad and Tobago",
      "city": "Port-of-Spain",
      "lat": 10.39,
      "long": -61.31
    },
    {
      "country": "Turkey",
      "city": "Ankara",
      "lat": 39.56,
      "long": 32.52
    },
    {
      "country": "Sri Lanka",
      "city": "Colombo",
      "lat": 6.56,
      "long": 79.51
    },
    {
      "country": "Liechtenstein",
      "city": "Vaduz",
      "lat": 47.08,
      "long": 9.31
    },
    {
      "country": "Latvia",
      "city": "Riga",
      "lat": 56.57,
      "long": 24.06
    },
    {
      "country": "Tonga",
      "city": "Nuku\'alofa",
      "lat": -21.08,
      "long": -175.12
    },
    {
      "country": "Lithuania",
      "city": "Vilnius",
      "lat": 54.41,
      "long": 25.19
    },
    {
      "country": "Luxembourg",
      "city": "Luxembourg",
      "lat": 49.36,
      "long": 6.07
    },
    {
      "country": "Liberia",
      "city": "Monrovia",
      "lat": 6.18,
      "long": -10.48
    },
    {
      "country": "Lesotho",
      "city": "Maseru",
      "lat": -29.19,
      "long": 27.29
    },
    {
      "country": "Thailand",
      "city": "Bangkok",
      "lat": 13.45,
      "long": 100.31
    },
    {
      "country": "French Southern Territories",
      "city": null,
      "lat": -37.5,
      "long": 77.32
    },
    {
      "country": "Togo",
      "city": "Lome",
      "lat": 6.08,
      "long": 1.13
    },
    {
      "country": "Chad",
      "city": "N\'Djamena",
      "lat": 12.06,
      "long": 15.02
    },
    {
      "country": "Turks and Caicos Islands",
      "city": "Grand Turk",
      "lat": 21.28,
      "long": -71.08
    },
    {
      "country": "Libya",
      "city": "Tripoli",
      "lat": 32.53,
      "long": 13.1
    },
    {
      "country": "Holy See",
      "city": "Vatican City",
      "lat": 41.54,
      "long": 12.27
    },
    {
      "country": "Saint Vincent and the Grenadines",
      "city": "Kingstown",
      "lat": 13.09,
      "long": -61.14
    },
    {
      "country": "United Arab Emirates",
      "city": "Abu Dhabi",
      "lat": 24.28,
      "long": 54.22
    },
    {
      "country": "Andorra",
      "city": "Andorra la Vella",
      "lat": 42.3,
      "long": 1.31
    },
    {
      "country": "Antigua and Barbuda",
      "city": "Saint John\'s",
      "lat": 17.07,
      "long": -61.51
    },
    {
      "country": "Afghanistan",
      "city": "Kabul",
      "lat": 34.31,
      "long": 69.11
    },
    {
      "country": "Anguilla",
      "city": "The Valley",
      "lat": 18.13,
      "long": -63.03
    },
    {
      "country": "Virgin Islands",
      "city": "Charlotte Amalie",
      "lat": 18.21,
      "long": -64.56
    },
    {
      "country": "Iceland",
      "city": "Reykjavik",
      "lat": 64.09,
      "long": -21.57
    },
    {
      "country": "Iran",
      "city": "Tehran",
      "lat": 35.4,
      "long": 51.25
    },
    {
      "country": "Armenia",
      "city": "Yerevan",
      "lat": 40.1,
      "long": 44.3
    },
    {
      "country": "Albania",
      "city": "Tirana",
      "lat": 41.19,
      "long": 19.49
    },
    {
      "country": "Angola",
      "city": "Luanda",
      "lat": -8.5,
      "long": 13.14
    },
    {
      "country": "Netherlands Antilles",
      "city": "Willemstad",
      "lat": 12.06,
      "long": -68.56
    },
    {
      "country": "Antarctica",
      "city": null,
      "lat": -90,
      "long": 0
    },
    {
      "country": "Asia &amp; Pacific",
      "city": null,
      "lat": 0,
      "long": 0
    },
    {
      "country": "American Samoa",
      "city": "Pago Pago",
      "lat": -14.16,
      "long": -170.42
    },
    {
      "country": "Argentina",
      "city": "Buenos Aires",
      "lat": -34.36,
      "long": -58.4
    },
    {
      "country": "Australia",
      "city": "Canberra",
      "lat": -35.17,
      "long": 149.13
    },
    {
      "country": "Austria",
      "city": "Vienna",
      "lat": 48.12,
      "long": 16.22
    },
    {
      "country": "Aruba",
      "city": "Oranjestad",
      "lat": 12.31,
      "long": -70.02
    },
    {
      "country": "India",
      "city": "New Delhi",
      "lat": 28.36,
      "long": 77.12
    },
    {
      "country": "Tanzania",
      "city": "Dar es Salaam",
      "lat": -6.48,
      "long": 39.17
    },
    {
      "country": "Azerbaijan",
      "city": "Baku",
      "lat": 40.23,
      "long": 49.52
    },
    {
      "country": "Ireland",
      "city": "Dublin",
      "lat": 53.19,
      "long": -6.14
    },
    {
      "country": "Indonesia",
      "city": "Jakarta",
      "lat": -6.1,
      "long": 106.49
    },
    {
      "country": "Ukraine",
      "city": "Kiew",
      "lat": 50.441,
      "long": 30.554
    },
    {
      "country": "Qatar",
      "city": "Doha",
      "lat": 25.17,
      "long": 51.32
    },
    {
      "country": "Mozambique",
      "city": "Maputo",
      "lat": -25.57,
      "long": 32.35
    }
  ]
}
';



?>