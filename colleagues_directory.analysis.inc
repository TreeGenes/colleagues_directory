<?php

/**
 * @file
 * Analysis module integration for the Colleagues Directory module.
 */

/**
 * Returns analysis form used for filtering
 *
 *
 * @return string
 *   The analysis form
 *
 */
function colleagues_directory_generate_analysis_form($form, &$form_state) {
	$form['workflow_name'] = array(
		'#type' => 'textfield',
		'#title' => t('Workflow Name'),
		'#value' => @$_SESSION['cdafd']['workflow_name'],
		'#required' => TRUE,
	);
	
	$form['submit_button'] = array(
		'#type' => 'submit',
		'#value' => t('Filter'),
	);
	
	$form['reset_button'] = array(
		'#type' => 'submit',
		'#value' => t('Reset'),
	);
	
	$form['#submit'][] = 'colleagues_directory_generate_analysis_form_dosubmit';
	
	return $form;
}


/**
 * Returns nothing but sets sessions variables for filtering analysis form
 *
 *
 * @return null
 *   Returns null but sets valuable session variables used to filter the analysis table page
 *
 */
function colleagues_directory_generate_analysis_form_dosubmit($form, $form_state) {
	//dpm($form_state);
	if($form_state['input']['op'] == 'Reset') {
		unset($_SESSION['cdafd']);
		drupal_set_message('Filters cleared!');
	}
	else {
		$_SESSION['cdafd']['workflow_name'] = @$form_state['input']['workflow_name'];
	}	
}



/**
 * Returns analysis page
 *
 *
 * @return string
 *   The analysis page HTML output
 *
 */
function colleagues_directory_generate_analysis_page() {
    global $user;
    $user_id = $user->uid;
	$module_path = drupal_get_path('module', 'colleagues_directory');	
	
	

	$form = drupal_get_form('colleagues_directory_generate_analysis_form');

	//dpm($_REQUEST);
	$content = '';
    $content .= drupal_render($form);

	
    // Header
    $header = array(
        array('data' => 'Workflow Name',  'field' => 'workflow_name'),
        array('data' => 'Status',  'field' => 'status'),
		array('data' => 'Details',  'field' => 'n.nid'),
    );
    

    // Query
    $query = db_select('tripal_galaxy_workflow','tg_workflow');

	$query->join('node','n','tg_workflow.nid = n.nid');

	$query->fields('tg_workflow', array('nid', 'workflow_name', 'status'));
	$query->fields('n', array('uid'));
	$query->condition('uid', $user_id, '=');
	
	//dpm($_SESSION);
	if(@$_SESSION['cdafd']['workflow_name'] != '') {
		drupal_set_message('Filtering by Workflow Name');
		$workflow_name = @$_SESSION['cdafd']['workflow_name'];
		$query->condition('workflow_name', '%' . filter_xss(check_plain($workflow_name)) . '%', 'ILIKE');
	}
	
	$rows = $query->execute();

    // Build the table rows from the returned database data
    $rows_list = array();
	
    foreach($rows as $row) {
        //dpm($row);
		//echo $row;
        $rows_list[] = array(
			$row->workflow_name,
			$row->status,
			"<a href='/node/" . $row->nid . "'>Details</a>",
        );
    }	
	
	//dpm($rows_list);
	
    // Give the table a class if styling is desired 
    $results_table_attributes = array(
        'class' => array(
            'analysis-table',
        ),
    );
    
    // Let the users know they can sort the columns
    $results_table_caption = 'Sort columns by clicking on a header';
    
    // Build the table variable array
    $results_table_vars = array(
		'header'        => $header,
		'rows'          => $rows_list,
		'attributes'    => $results_table_attributes,
		//'caption'       => $results_table_caption
    );	
	
	$content .= theme('table',$results_table_vars);
	
	//return $content;
	$ret_content['raw_markup'] = array(
		'#type' => 'markup',
		'#markup' => $content,
	);	
	
	return $ret_content;
	
}



?>