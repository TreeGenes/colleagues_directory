<?php

/**
 * @file
 * Views module integration for the Colleagues Directory module.
 */


/**
 * Implements hook_views_api().
 */
function colleagues_directory_views_api() {
  return array(
    'api' => 3.0,
    'path' => drupal_get_path('module', 'colleagues_directory')
  );
}
 
/**
 * Implements hook_views_default_views().
 */
function colleagues_directory_views_default_views() {
 
  // Paste code form views export.
	//The Job View found at /colleagues_directory/jobs
	if(variable_get('colleagues_directory_enableview_jobs', 0) == 1) {
		$view = new view();
		$view->name = 'jobs_custom';
		$view->description = '';
		$view->tag = 'default';
		$view->base_table = 'node';
		$view->human_name = 'Jobs';
		$view->core = 7;
		$view->api_version = '3.0';
		$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

		/* Display: Master */
		$handler = $view->new_display('default', 'Master', 'default');
		$handler->display->display_options['title'] = 'Jobs';
		$handler->display->display_options['use_more_always'] = FALSE;
		$handler->display->display_options['access']['type'] = 'perm';
		$handler->display->display_options['cache']['type'] = 'none';
		$handler->display->display_options['query']['type'] = 'views_query';
		$handler->display->display_options['exposed_form']['type'] = 'basic';
		$handler->display->display_options['pager']['type'] = 'full';
		$handler->display->display_options['pager']['options']['items_per_page'] = '10';
		$handler->display->display_options['style_plugin'] = 'table';
		$handler->display->display_options['style_options']['columns'] = array(
		  'nid' => 'nid',
		  'job_position' => 'job_position',
		  'job_location' => 'job_location',
		  'job_institution' => 'job_institution',
		  'job_close_date' => 'job_close_date',
		);
		$handler->display->display_options['style_options']['default'] = '-1';
		$handler->display->display_options['style_options']['info'] = array(
		  'nid' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		  'job_position' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		  'job_location' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		  'job_institution' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		  'job_close_date' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		);
		$handler->display->display_options['style_options']['empty_table'] = TRUE;
		/* Header: Global: PHP */
		$handler->display->display_options['header']['php']['id'] = 'php';
		$handler->display->display_options['header']['php']['table'] = 'views';
		$handler->display->display_options['header']['php']['field'] = 'php';
		$handler->display->display_options['header']['php']['empty'] = TRUE;
		$handler->display->display_options['header']['php']['php_output'] = '<div style=\'padding: 5px; margin-bottom: 5px;\'>
		To post a new listing here (new employment opportunity or graduate student positions), please ensure you have a current account and post <b><a href=\'/node/add/job\'>here</a></b>.
		</div>
		';
		/* No results behavior: Global: Text area */
		$handler->display->display_options['empty']['area']['id'] = 'area';
		$handler->display->display_options['empty']['area']['table'] = 'views';
		$handler->display->display_options['empty']['area']['field'] = 'area';
		$handler->display->display_options['empty']['area']['empty'] = TRUE;
		$handler->display->display_options['empty']['area']['content'] = 'No jobs found';
		$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
		/* Field: Content: Nid */
		$handler->display->display_options['fields']['nid']['id'] = 'nid';
		$handler->display->display_options['fields']['nid']['table'] = 'node';
		$handler->display->display_options['fields']['nid']['field'] = 'nid';
		$handler->display->display_options['fields']['nid']['label'] = '';
		$handler->display->display_options['fields']['nid']['exclude'] = TRUE;
		$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
		/* Field: Content: Position */
		$handler->display->display_options['fields']['job_position']['id'] = 'job_position';
		$handler->display->display_options['fields']['job_position']['table'] = 'field_data_job_position';
		$handler->display->display_options['fields']['job_position']['field'] = 'job_position';
		$handler->display->display_options['fields']['job_position']['alter']['make_link'] = TRUE;
		$handler->display->display_options['fields']['job_position']['alter']['path'] = 'node/[nid]';
		$handler->display->display_options['fields']['job_position']['settings'] = array(
		  'field_formatter_class' => '',
		);
		/* Field: Content: Location */
		$handler->display->display_options['fields']['job_location']['id'] = 'job_location';
		$handler->display->display_options['fields']['job_location']['table'] = 'field_data_job_location';
		$handler->display->display_options['fields']['job_location']['field'] = 'job_location';
		$handler->display->display_options['fields']['job_location']['settings'] = array(
		  'field_formatter_class' => '',
		);
		/* Field: Content: Institution */
		$handler->display->display_options['fields']['job_institution']['id'] = 'job_institution';
		$handler->display->display_options['fields']['job_institution']['table'] = 'field_data_job_institution';
		$handler->display->display_options['fields']['job_institution']['field'] = 'job_institution';
		$handler->display->display_options['fields']['job_institution']['settings'] = array(
		  'field_formatter_class' => '',
		);
		/* Field: Content: Close Date */
		$handler->display->display_options['fields']['job_close_date']['id'] = 'job_close_date';
		$handler->display->display_options['fields']['job_close_date']['table'] = 'field_data_job_close_date';
		$handler->display->display_options['fields']['job_close_date']['field'] = 'job_close_date';
		$handler->display->display_options['fields']['job_close_date']['settings'] = array(
		  'format_type' => 'long',
		  'custom_date_format' => '',
		  'fromto' => 'both',
		  'multiple_number' => '',
		  'multiple_from' => '',
		  'multiple_to' => '',
		  'show_remaining_days' => 0,
		  'field_formatter_class' => '',
		);
		/* Sort criterion: Content: Post date */
		$handler->display->display_options['sorts']['created']['id'] = 'created';
		$handler->display->display_options['sorts']['created']['table'] = 'node';
		$handler->display->display_options['sorts']['created']['field'] = 'created';
		$handler->display->display_options['sorts']['created']['order'] = 'DESC';
		/* Filter criterion: Content: Published */
		$handler->display->display_options['filters']['status']['id'] = 'status';
		$handler->display->display_options['filters']['status']['table'] = 'node';
		$handler->display->display_options['filters']['status']['field'] = 'status';
		$handler->display->display_options['filters']['status']['value'] = 1;
		$handler->display->display_options['filters']['status']['group'] = 1;
		$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
		/* Filter criterion: Content: Type */
		$handler->display->display_options['filters']['type']['id'] = 'type';
		$handler->display->display_options['filters']['type']['table'] = 'node';
		$handler->display->display_options['filters']['type']['field'] = 'type';
		$handler->display->display_options['filters']['type']['value'] = array(
		  'job' => 'job',
		);
		/* Filter criterion: Content: Job Type (job_type) */
		$handler->display->display_options['filters']['job_type_value']['id'] = 'job_type_value';
		$handler->display->display_options['filters']['job_type_value']['table'] = 'field_data_job_type';
		$handler->display->display_options['filters']['job_type_value']['field'] = 'job_type_value';
		$handler->display->display_options['filters']['job_type_value']['exposed'] = TRUE;
		$handler->display->display_options['filters']['job_type_value']['expose']['operator_id'] = 'job_type_value_op';
		$handler->display->display_options['filters']['job_type_value']['expose']['label'] = 'Job Type';
		$handler->display->display_options['filters']['job_type_value']['expose']['operator'] = 'job_type_value_op';
		$handler->display->display_options['filters']['job_type_value']['expose']['identifier'] = 'job_type_value';
		$handler->display->display_options['filters']['job_type_value']['expose']['remember_roles'] = array(
		  2 => '2',
		  1 => 0,
		  4 => 0,
		  3 => 0,
		  5 => 0,
		);

		/* Display: Page */
		$handler = $view->new_display('page', 'Page', 'page');
		$handler->display->display_options['path'] = 'colleagues_directory/jobs';

		/* Display: Block */
		$handler = $view->new_display('block', 'Block', 'block');
		$handler->display->display_options['defaults']['title'] = FALSE;
		$handler->display->display_options['title'] = 'Job postings';
		$handler->display->display_options['defaults']['pager'] = FALSE;
		$handler->display->display_options['pager']['type'] = 'some';
		$handler->display->display_options['pager']['options']['items_per_page'] = '2';

		 
	 
		//Add it to the views variable to be returned
		$views[$view->name] = $view;
	}
	
	if (variable_get('colleagues_directory_enableview_meetings', 0) == 1) {
		//This is for Meetings view /colleagues_directory/meetings
		$view = new view();
		$view->name = 'meetings_custom';
		$view->description = '';
		$view->tag = 'default';
		$view->base_table = 'node';
		$view->human_name = 'Meetings';
		$view->core = 7;
		$view->api_version = '3.0';
		$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

		/* Display: Master */
		$handler = $view->new_display('default', 'Master', 'default');
		$handler->display->display_options['title'] = 'Meetings';
		$handler->display->display_options['use_more_always'] = FALSE;
		$handler->display->display_options['access']['type'] = 'perm';
		$handler->display->display_options['cache']['type'] = 'none';
		$handler->display->display_options['query']['type'] = 'views_query';
		$handler->display->display_options['exposed_form']['type'] = 'basic';
		$handler->display->display_options['pager']['type'] = 'full';
		$handler->display->display_options['pager']['options']['items_per_page'] = '10';
		$handler->display->display_options['pager']['options']['offset'] = '0';
		$handler->display->display_options['pager']['options']['id'] = '0';
		$handler->display->display_options['pager']['options']['quantity'] = '9';
		$handler->display->display_options['style_plugin'] = 'table';
		$handler->display->display_options['style_options']['columns'] = array(
		  'field_conference_url' => 'field_conference_url',
		  'field_city' => 'field_city',
		  'title' => 'title',
		  'field_country' => 'field_country',
		  'field_event_date' => 'field_event_date',
		);
		$handler->display->display_options['style_options']['default'] = 'field_event_date';
		$handler->display->display_options['style_options']['info'] = array(
		  'field_conference_url' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		  'field_city' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		  'title' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		  'field_country' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		  'field_event_date' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		  ),
		);
		/* Header: Global: PHP */
		$handler->display->display_options['header']['php']['id'] = 'php';
		$handler->display->display_options['header']['php']['table'] = 'views';
		$handler->display->display_options['header']['php']['field'] = 'php';
		$handler->display->display_options['header']['php']['empty'] = TRUE;
		$handler->display->display_options['header']['php']['php_output'] = '<div style=\'padding: 5px; margin-bottom: 5px;\'>
		To post a new conference, meeting, or workshop listing,  please ensure you have a current account and post <b><a href=\'/node/add/meeting\'>here</a></b>.
		</div>';
		/* No results behavior: Global: Text area */
		$handler->display->display_options['empty']['area']['id'] = 'area';
		$handler->display->display_options['empty']['area']['table'] = 'views';
		$handler->display->display_options['empty']['area']['field'] = 'area';
		$handler->display->display_options['empty']['area']['empty'] = TRUE;
		$handler->display->display_options['empty']['area']['content'] = 'No meetings found';
		$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
		/* Field: Content: Conference URL */
		$handler->display->display_options['fields']['meeting_conf_url']['id'] = 'meeting_conf_url';
		$handler->display->display_options['fields']['meeting_conf_url']['table'] = 'field_data_meeting_conf_url';
		$handler->display->display_options['fields']['meeting_conf_url']['field'] = 'meeting_conf_url';
		$handler->display->display_options['fields']['meeting_conf_url']['exclude'] = TRUE;
		$handler->display->display_options['fields']['meeting_conf_url']['click_sort_column'] = 'url';
		$handler->display->display_options['fields']['meeting_conf_url']['type'] = 'link_plain';
		$handler->display->display_options['fields']['meeting_conf_url']['settings'] = array(
		  'field_formatter_class' => '',
		);
		/* Field: Content: City */
		$handler->display->display_options['fields']['meeting_city']['id'] = 'meeting_city';
		$handler->display->display_options['fields']['meeting_city']['table'] = 'field_data_meeting_city';
		$handler->display->display_options['fields']['meeting_city']['field'] = 'meeting_city';
		$handler->display->display_options['fields']['meeting_city']['exclude'] = TRUE;
		$handler->display->display_options['fields']['meeting_city']['settings'] = array(
		  'field_formatter_class' => '',
		);
		/* Field: Content: Title */
		$handler->display->display_options['fields']['title_1']['id'] = 'title_1';
		$handler->display->display_options['fields']['title_1']['table'] = 'node';
		$handler->display->display_options['fields']['title_1']['field'] = 'title';
		$handler->display->display_options['fields']['title_1']['label'] = 'Meeting';
		$handler->display->display_options['fields']['title_1']['alter']['make_link'] = TRUE;
		$handler->display->display_options['fields']['title_1']['alter']['path'] = '[meeting_conf_url] ';
		$handler->display->display_options['fields']['title_1']['alter']['external'] = TRUE;
		$handler->display->display_options['fields']['title_1']['link_to_node'] = FALSE;
		$handler->display->display_options['fields']['title_1']['node_in_colorbox_width'] = '600';
		$handler->display->display_options['fields']['title_1']['node_in_colorbox_height'] = '600';
		$handler->display->display_options['fields']['title_1']['node_in_colorbox_rel'] = '';
		/* Field: Content: Country */
		$handler->display->display_options['fields']['meeting_country']['id'] = 'meeting_country';
		$handler->display->display_options['fields']['meeting_country']['table'] = 'field_data_meeting_country';
		$handler->display->display_options['fields']['meeting_country']['field'] = 'meeting_country';
		$handler->display->display_options['fields']['meeting_country']['alter']['alter_text'] = TRUE;
		$handler->display->display_options['fields']['meeting_country']['alter']['text'] = '[meeting_city] [meeting_country]';
		$handler->display->display_options['fields']['meeting_country']['settings'] = array(
		  'field_formatter_class' => '',
		);
		/* Field: Content: Meeting Dates */
		$handler->display->display_options['fields']['meeting_meeting_dates']['id'] = 'meeting_meeting_dates';
		$handler->display->display_options['fields']['meeting_meeting_dates']['table'] = 'field_data_meeting_meeting_dates';
		$handler->display->display_options['fields']['meeting_meeting_dates']['field'] = 'meeting_meeting_dates';
		$handler->display->display_options['fields']['meeting_meeting_dates']['exclude'] = TRUE;
		$handler->display->display_options['fields']['meeting_meeting_dates']['settings'] = array(
		  'format_type' => 'custom',
		  'custom_date_format' => 'jS M Y',
		  'fromto' => 'both',
		  'multiple_number' => '',
		  'multiple_from' => '',
		  'multiple_to' => '',
		  'show_remaining_days' => 0,
		  'field_formatter_class' => '',
		);
		/* Field: Global: PHP */
		$handler->display->display_options['fields']['php']['id'] = 'php';
		$handler->display->display_options['fields']['php']['table'] = 'views';
		$handler->display->display_options['fields']['php']['field'] = 'php';
		$handler->display->display_options['fields']['php']['label'] = 'Meeting Dates';
		$handler->display->display_options['fields']['php']['use_php_setup'] = 0;
		$handler->display->display_options['fields']['php']['php_output'] = '<?php
		//dpm($row->field_event_date);
		//dpm($data->field_field_event_date);
		//$val = $data->field_event_date[0][\'raw\'][\'value\'];
		$d1 = explode(" ", $data->field_meeting_meeting_dates[0][\'raw\'][\'value\'])[0];
		$d2 = explode(" ", $data->field_meeting_meeting_dates[0][\'raw\'][\'value2\'])[0];

		$d1_parts = explode("-", $d1);
		//dpm($d1_parts);

		$d2_parts = explode("-", $d2);
		//dpm($d2_parts);

		if($d1_parts[0] == $d2_parts[0] && $d1_parts[0] != "") {
		   //year is the same
		   $time1 = mktime(0,0,0,$d1_parts[1], $d1_parts[2], $d1_parts[0]);
		   $time2 = mktime(0,0,0,$d2_parts[1], $d2_parts[2], $d2_parts[0]);
		   echo date("jS M", $time1) . " - " . date("jS M", $time2) . " " . date("Y", $time1);
		}
		else {
		   $time1 = mktime(0,0,0,$d1_parts[1], $d1_parts[2], $d1_parts[0]);
		   $time2 = mktime(0,0,0,$d2_parts[1], $d2_parts[2], $d2_parts[0]);
		   echo date("jS M Y", $time1) . " - " . date("jS M Y", $time2);
		}

		echo $val;
		?>';
		$handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
		$handler->display->display_options['fields']['php']['php_click_sortable'] = '';
		/* Sort criterion: Content: Meeting Dates -  start date (meeting_meeting_dates) */
		$handler->display->display_options['sorts']['meeting_meeting_dates_value']['id'] = 'meeting_meeting_dates_value';
		$handler->display->display_options['sorts']['meeting_meeting_dates_value']['table'] = 'field_data_meeting_meeting_dates';
		$handler->display->display_options['sorts']['meeting_meeting_dates_value']['field'] = 'meeting_meeting_dates_value';
		/* Filter criterion: Content: Published */
		$handler->display->display_options['filters']['status']['id'] = 'status';
		$handler->display->display_options['filters']['status']['table'] = 'node';
		$handler->display->display_options['filters']['status']['field'] = 'status';
		$handler->display->display_options['filters']['status']['value'] = 1;
		$handler->display->display_options['filters']['status']['group'] = 1;
		$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
		/* Filter criterion: Content: Type */
		$handler->display->display_options['filters']['type']['id'] = 'type';
		$handler->display->display_options['filters']['type']['table'] = 'node';
		$handler->display->display_options['filters']['type']['field'] = 'type';
		$handler->display->display_options['filters']['type']['value'] = array(
		  'meeting' => 'meeting',
		);
		/* Filter criterion: Content: Meeting Dates -  start date (meeting_meeting_dates) */
		$handler->display->display_options['filters']['meeting_meeting_dates_value']['id'] = 'meeting_meeting_dates_value';
		$handler->display->display_options['filters']['meeting_meeting_dates_value']['table'] = 'field_data_meeting_meeting_dates';
		$handler->display->display_options['filters']['meeting_meeting_dates_value']['field'] = 'meeting_meeting_dates_value';
		$handler->display->display_options['filters']['meeting_meeting_dates_value']['operator'] = '>=';
		$handler->display->display_options['filters']['meeting_meeting_dates_value']['default_date'] = 'now';
		$handler->display->display_options['filters']['meeting_meeting_dates_value']['year_range'] = '-0:+3';

		/* Display: Page */
		$handler = $view->new_display('page', 'Page', 'page');
		$handler->display->display_options['path'] = 'colleagues_directory/meetings';

		//Add it to the views variable to be returned
		$views[$view->name] = $view;
	}


	return $views;
} 

?>