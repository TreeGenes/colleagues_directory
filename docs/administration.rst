Administration
==============

The administration pages for Colleagues Directory can be found at /admin/tripal/colleagues_directory/install

There are 4 tab options with various configuration settings

* Install / Uninstall
* CD Settings
* Welcome Settings
* Welcome Items

TAB: Install / Uninstall
------------------------
This tab has most of the vital options required to produce a community section. It contains

* Install Meeting Content Type

This button will automatically create a Drupal contact type called meeting with basic fields related to meetings. 

* Install Job Content Type

This button will automatically create a Drupal contact type called job with basic fields related to jobs.

* Enable default meetings view

This will create an actual view under admin -> structures which you can use to display all meetings created.

* Enable default jobs view

This will create an actual view under admin -> structures which you can use to display all jobs created.

TAB: CD Settings (Colleagues Directory Settings)
------------------------------------------------

* Set Site Name Label

You can optionally change the site name text within the colleagues directory page that lists the contacts

* Show Photo field column in Colleagues Directory

This will make the first column of the colleagues directory display photos of the specific contacts assuming that you enabled the photos field in Tripal 
Contact Profile.

* Show Hobbies field column in Colleagues Directory

This will show the hobbies field data once you created the hobbies field. Read the instructions on how to do that within the UI page.

* MapBox API Key

Enter a valid MapBox API Key before enabling the map. This is required in order to render the Mapbox Map.

* Show Map in Colleagues Directory

This will toggle whether the map is seen within the Colleagues Directory page.

TAB: Welcome Settings
---------------------

* Enable welcome page

This will create a welcome page at /welcome

* Welcome items

Enter the number of items you would like created on the welcome page. After you save this, you can then head over to the Welcome Items tab to configure 
the welcome items.

TAB: Welcome Items
------------------

* EDIT PROFILE Items

This is a required item which will point to a users edit profile link. This allows a user to edit their profile or create a TCP profile if one does not 
exist.


* EDIT PASSWORD ITEM

This item points to the section where a user can edit their password. 

* EDIT WELCOME ITEM #

All the rest of welcome items are customizable based on the settings you enter into these fields.

