Installation
============

To successfully install Colleagues Directory, you should install the following requirements

* Tripal v3
* Chado v1.3
* Tripal Contact Profile
* Obtain a Mapbox API key (for Colleagues Directory mapping features)

After you have installed  Tripal v3, Chado v1.3 and TCP

Make sure to set up all TCP requirements since this will set up the TCP Tripal Contact Profile which will be used by Colleagues Directory.

Continue by copying the files in the Colleagues Directory into your sites/all/modules folder such that it looks like 
/sites/all/modules/colleagues_directory/

Then head over to your site's administration modules page and enable Colleagues Directory

Once enabled, you can go to Admin -> Tripal -> Colleagues Directory Configuration and set it up with your preferences.