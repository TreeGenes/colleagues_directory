.. Colleagues Directory documentation master file, created by
   sphinx-quickstart on Mon Aug 12 16:22:28 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Colleagues Directory's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   administration
   customization


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
