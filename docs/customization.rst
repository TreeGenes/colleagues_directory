Customization
=============

Parts of Colleagues Directory can be further customized to meet your needs. 

* Meetings

Since the Meetings directory is actually created using the Views API, administrators can add additional fields to the meetings content type 
which can thus extend the meetings features. Once new fields are added to the meetings field, admins can then edit the corresponding view 
accordingly.

* Jobs
Since the Jobs directory is actually created using the Views API, administrators can add additional fields to the jobs content type 
which can thus extend the jobs features. Once new fields are added to the jobs field, admins can then edit the corresponding view 
accordingly.