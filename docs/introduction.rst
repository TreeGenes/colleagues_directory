Introduction
============

Colleagues Directory is a supplemental module used in conjunction with Tripal Contact Profile to produce a community section on any 
Tripal v3 website. It creates the colleagues directory, a jobs directory and also a meetings directory. It does so by utilizing the 
Tripal Contact Profile module for the colleagues directory, creating a job content type for jobs directory and a meeting content type 
for meetings directory utilizing the View modules (usually already installed on Tripal sites). It also includes a welcome page generator 
which is configurable via the Colleagues Directory Configuration options found in the Tripal Administration User Interface. See Administration 
for more details.

