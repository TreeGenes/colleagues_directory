<?php

/**
 * @file
 * Ajax module integration for the Colleagues Directory module.
 */

/**
 * Returns US states geo points
 *
 *
 * @return null
 *   Returns null but outputs JSON code directly to page
 *
 */
function colleagues_directory_ajax_contacts_us_states_geopoints() {
	global $us_states_geo_json;
	$us_states_geo_json_arr = json_decode($us_states_geo_json);
	
	//This code is going to build 2 index php variables based on abbr or name to make search faster, since we need this data often
	$search_state_by_abbr = array();
	$search_state_by_name = array();
	foreach($us_states_geo_json_arr->states as $state_details) {
		//dpm($state_details);
		$abbr = strtolower($state_details->abbr);
		$name = strtolower($state_details->name);
		$lat = $state_details->lat;
		$long = $state_details->long;
		
		//Add to search variables
		$search_state_by_abbr[$abbr] = array(
			'abbr' => $abbr,
			'name' => $name,
			'lat' => $lat,
			'long' => $long,
		);
		
		$search_state_by_name[$name] = array(
			'abbr' => $abbr,
			'name' => $name,
			'lat' => $lat,
			'long' => $long,
		);		
	}
	

	//Get the type_id for states since we need this to get contactprop data from this
	$args = array();
	$typeid_state_record = chado_query("SELECT cvterm_id, * FROM chado.cvterm WHERE name LIKE 'State' LIMIT 1;", $args);
	$typeid_state = -1;
	foreach($typeid_state_record as $temp_row) {
		$typeid_state = $temp_row->cvterm_id;
	}
	
	$args = array();
	$typeid_person_record = chado_query("SELECT cvterm_id, * FROM chado.cvterm WHERE name LIKE 'Person' LIMIT 1;", $args);
	$typeid_person = -1;
	foreach($typeid_person_record as $temp_row) {
		$typeid_person = $temp_row->cvterm_id;
	}	
	
	//echo($typeid_state);
	
	

	
	$us_states_count_records = chado_query('SELECT COUNT(value) as c, value FROM (SELECT * FROM chado.contactprop JOIN chado.contact ON 
	chado.contact.contact_id = chado.contactprop.contact_id WHERE contactprop.type_id = ' . $typeid_state. ' AND contact.type_id = ' . $typeid_person . ') 
	AS table1 GROUP BY value;', $args);

	
	//Now generate GeoJSON
	$map_records = array('type'=> 'FeatureCollection');
	$unique_count = 0;
	foreach($us_states_count_records as $record){
		//echo($record);
		$count = $record->c;
		$value = strtolower($record->value);
		
		for($i=0; $i < $count; $i++) {
			$new_geopoint = array(
				'type' => 'Feature',
				'properties' => array(
					'coordinate_type' => 'exact'  
				),
				'geometry' => array(
					'type' => 'Point' 
				)
			);
			
			//determine if the value has a search record in $search_state_by_abbr or $search_state_by_name
			$add_record = true;
			if($search_state_by_abbr[$value] != NULL) {
				$new_geopoint['geometry']['coordinates'] = array($search_state_by_abbr[$value]['long'], $search_state_by_abbr[$value]['lat']);
				$new_geopoint['properties']['abbr'] = $value;
				$new_geopoint['properties']['name'] = $search_state_by_abbr[$value]['name'];
			}
			else if($search_state_by_name[$value] != NULL) {
				$new_geopoint['geometry']['coordinates'] = array($search_state_by_name[$value]['long'], $search_state_by_name[$value]['lat']);
				$new_geopoint['properties']['name'] = $value;
				$new_geopoint['properties']['abbr'] = $search_state_by_name[$value]['abbr'];
			}
			else {
				//this is not a good record
				$add_record = false;
			}
			
			if($add_record) {
				$unique_count = $unique_count + 1;
				$new_geopoint['id'] = $unique_count;
				$map_records['features'][] = $new_geopoint; //push to map_records array
			}
		}
		
		/*
		if(!in_array($record->uniquename, $exclude)){
			$curr_tree = array(
				'type' => 'Feature',
				'properties' => array(
					'coordinate_type' => 'exact'  
				),
				'geometry' => array(
					'type' => 'Point' 
				)
			);
			$prop = &$curr_tree['properties'];
			$prop['id'] = $record->uniquename;
			$prop['species'] = $record->genus . ' ' .$record->species;
			$prop['data_source'] = 'treegenes';
			$prop['markertype'] = $record->marker_type;
			$prop['pub_tgdr'] = $record->accession;
			$prop['phenotype_name'] = $record->phenotype_name;
			$prop['pato'] = $record->pato_name;
			$prop['po'] = $record->po_name;
			$prop['genus'] = $record->genus;
			$prop['pub_year'] = $record->pyear;
			$prop['plant_group'] = $record->subkingdom;
			$prop['family'] = $record->family;
			$curr_tree['geometry']['coordinates'] = array($record->longitude, $record->latitude);
			$curr_tree['id'] = $record->geometry;
			$trees['features'][] = $curr_tree; //push to trees array
		}*/
	}
	drupal_json_output($map_records);
}

/**
 * Returns Canada Provinces geo points
 *
 *
 * @return null
 *   Returns null but outputs JSON code directly to page
 *
 */
function colleagues_directory_ajax_contacts_canada_provinces_geopoints() {
	global $canada_provinces_geo_json;
	$canada_provinces_geo_json_arr = json_decode($canada_provinces_geo_json);
	//var_dump($canada_provinces_geo_json_arr);
	//This code is going to build 2 index php variables based on abbr or name to make search faster, since we need this data often
	$search_provinces_by_abbr = array();
	$search_provinces_by_name = array();
	foreach($canada_provinces_geo_json_arr->provinces as $province_details) {
		//dpm($state_details);
		//var_dump($province_details);
		$abbr = strtolower($province_details->short);
		$name = strtolower($province_details->name);
		$lat = $province_details->latitude;
		$long = $province_details->longitude;
		
		//Add to search variables
		$search_provinces_by_abbr[$abbr] = array(
			'abbr' => $abbr,
			'name' => $name,
			'lat' => $lat,
			'long' => $long,
		);
		
		$search_provinces_by_name[$name] = array(
			'abbr' => $abbr,
			'name' => $name,
			'lat' => $lat,
			'long' => $long,
		);		
	}
	

	//Get the type_id for states since we need this to get contactprop data from this
	$args = array();
	$typeid_country_record = chado_query("SELECT cvterm_id, * FROM chado.cvterm WHERE name LIKE 'Country' LIMIT 1;", $args);
	$typeid_country = -1;
	foreach($typeid_country_record as $temp_row) {
		$typeid_country = $temp_row->cvterm_id;
	}	
	
	//Get the type_id for states (also provinces since this is Canada) since we need this to get contactprop data from this
	$args = array();
	$typeid_state_record = chado_query("SELECT cvterm_id, * FROM chado.cvterm WHERE name LIKE 'State' LIMIT 1;", $args);
	$typeid_state = -1;
	foreach($typeid_state_record as $temp_row) {
		$typeid_state = $temp_row->cvterm_id;
	}
	
	$args = array();
	$typeid_person_record = chado_query("SELECT cvterm_id, * FROM chado.cvterm WHERE name LIKE 'Person' LIMIT 1;", $args);
	$typeid_person = -1;
	foreach($typeid_person_record as $temp_row) {
		$typeid_person = $temp_row->cvterm_id;
	}	
	
	//echo($typeid_state);
	
	

	
	$canada_provinces_count_records = chado_query('SELECT COUNT(value) as c, value FROM (SELECT * FROM chado.contactprop JOIN chado.contact ON 
	chado.contact.contact_id = chado.contactprop.contact_id WHERE contactprop.type_id = ' . $typeid_state. ' AND contact.type_id = ' . $typeid_person . ') 
	AS table1 GROUP BY value;', $args);

	
	//Now generate GeoJSON
	$map_records = array('type'=> 'FeatureCollection');
	$unique_count = 0;
	foreach($canada_provinces_count_records as $record){
		//echo($record);
		//var_dump($record);
		$count = $record->c;
		$value = strtolower($record->value);
		
		for($i=0; $i < $count; $i++) {
			$new_geopoint = array(
				'type' => 'Feature',
				'properties' => array(
					'coordinate_type' => 'exact'  
				),
				'geometry' => array(
					'type' => 'Point' 
				)
			);
			
			//determine if the value has a search record in $search_provinces_by_abbr or $search_provinces_by_name
			$add_record = true;
			if($search_provinces_by_abbr[$value] != NULL) {
				$new_geopoint['geometry']['coordinates'] = array($search_provinces_by_abbr[$value]['long'], $search_provinces_by_abbr[$value]['lat']);
				$new_geopoint['properties']['abbr'] = $value;
				$new_geopoint['properties']['name'] = $search_provinces_by_abbr[$value]['name'];
			}
			else if($search_provinces_by_name[$value] != NULL) {
				$new_geopoint['geometry']['coordinates'] = array($search_provinces_by_name[$value]['long'], $search_provinces_by_name[$value]['lat']);
				$new_geopoint['properties']['name'] = $value;
				$new_geopoint['properties']['abbr'] = $search_provinces_by_name[$value]['abbr'];
			}
			else {
				//this is not a good record
				$add_record = false;
			}
			
			if($add_record) {
				$unique_count = $unique_count + 1;
				$new_geopoint['id'] = $unique_count;
				$map_records['features'][] = $new_geopoint; //push to map_records array
			}
		}
		
		/*
		if(!in_array($record->uniquename, $exclude)){
			$curr_tree = array(
				'type' => 'Feature',
				'properties' => array(
					'coordinate_type' => 'exact'  
				),
				'geometry' => array(
					'type' => 'Point' 
				)
			);
			$prop = &$curr_tree['properties'];
			$prop['id'] = $record->uniquename;
			$prop['species'] = $record->genus . ' ' .$record->species;
			$prop['data_source'] = 'treegenes';
			$prop['markertype'] = $record->marker_type;
			$prop['pub_tgdr'] = $record->accession;
			$prop['phenotype_name'] = $record->phenotype_name;
			$prop['pato'] = $record->pato_name;
			$prop['po'] = $record->po_name;
			$prop['genus'] = $record->genus;
			$prop['pub_year'] = $record->pyear;
			$prop['plant_group'] = $record->subkingdom;
			$prop['family'] = $record->family;
			$curr_tree['geometry']['coordinates'] = array($record->longitude, $record->latitude);
			$curr_tree['id'] = $record->geometry;
			$trees['features'][] = $curr_tree; //push to trees array
		}*/
	}
	drupal_json_output($map_records);
}

/**
 * Returns World Capitals GeoPoints
 *
 *
 * @return null
 *   Returns null but outputs JSON directly to page
 *
 */
function colleagues_directory_ajax_contacts_world_capitals_geopoints() {
	global $world_capitals_geo_json;
	$world_capitals_geo_json_arr = json_decode($world_capitals_geo_json);
	
	//This code is going to build 2 index php variables based on country or city to make search faster, since we need this data often
	$search_capitals_by_country = array();
	$search_captails_by_city = array();
	foreach($world_capitals_geo_json_arr->capitals as $capital_details) {
		//echo($capital_details);
		$country = strtolower($capital_details->country);
		$city = strtolower($capital_details->city);
		//echo $country . " " . $city . "\n";
		$lat = $capital_details->lat;
		$long = $capital_details->long;
		
		//Add to search variables
		$search_capitals_by_country[$country] = array(
			'country' => $country,
			'city' => $city,
			'lat' => $lat,
			'long' => $long,
		);
		
		$search_capitals_by_city[$city] = array(
			'country' => $country,
			'city' => $city,
			'lat' => $lat,
			'long' => $long,
		);		
	}
	
	//echo count($search_capitals_by_country);

	//Get the type_id for states since we need this to get contactprop data from this
	$args = array();
	$typeid_state_record = chado_query("SELECT cvterm_id, * FROM chado.cvterm WHERE name LIKE 'Country' LIMIT 1;", $args);
	$typeid_state = -1;
	foreach($typeid_state_record as $temp_row) {
		$typeid_state = $temp_row->cvterm_id;
	}
	
	
	$args = array();
	$typeid_person_record = chado_query("SELECT cvterm_id, * FROM chado.cvterm WHERE name LIKE 'Person' LIMIT 1;", $args);
	$typeid_person = -1;
	foreach($typeid_person_record as $temp_row) {
		$typeid_person = $temp_row->cvterm_id;
	}
	
	//echo($typeid_state);
	
	

	
	$capital_count_records = chado_query('SELECT COUNT(value) as c, value FROM (SELECT * FROM chado.contactprop JOIN chado.contact ON chado.contact.contact_id = chado.contactprop.contact_id 
		WHERE contactprop.type_id = ' . $typeid_state . ' AND contact.type_id = ' . $typeid_person . ' AND value NOT ILIKE \'United States\' AND value NOT ILIKE \'Canada\') as table1 GROUP BY value;', $args);

	
	//Now generate GeoJSON
	$map_records = array('type'=> 'FeatureCollection');
	$unique_count = 0;
	foreach($capital_count_records as $record){
		//echo($record);
		$count = $record->c;
		$value = strtolower($record->value);
		
		for($i=0; $i < $count; $i++) {
			$new_geopoint = array(
				'type' => 'Feature',
				'properties' => array(
					'coordinate_type' => 'exact'  
				),
				'geometry' => array(
					'type' => 'Point' 
				)
			);
			
			//determine if the value has a search record in $search_capitals_by_country or $search_captails_by_city
			$add_record = true;
			if($search_capitals_by_country[$value] != NULL) {
				$new_geopoint['geometry']['coordinates'] = array($search_capitals_by_country[$value]['long'], $search_capitals_by_country[$value]['lat']);
				$new_geopoint['properties']['country'] = $value;
				$new_geopoint['properties']['city'] = $search_capitals_by_country[$value]['city'];
			}
			else if($search_captails_by_city[$value] != NULL) {
				$new_geopoint['geometry']['coordinates'] = array($search_captails_by_city[$value]['long'], $search_captails_by_city[$value]['lat']);
				$new_geopoint['properties']['city'] = $value;
				$new_geopoint['properties']['country'] = $search_captails_by_city[$value]['country'];
			}
			else {
				//this is not a good record
				$add_record = false;
			}
			
			if($add_record) {
				$unique_count = $unique_count + 1;
				$new_geopoint['id'] = $unique_count;
				$map_records['features'][] = $new_geopoint; //push to map_records array
			}
		}
		
		/*
		if(!in_array($record->uniquecity, $exclude)){
			$curr_tree = array(
				'type' => 'Feature',
				'properties' => array(
					'coordinate_type' => 'exact'  
				),
				'geometry' => array(
					'type' => 'Point' 
				)
			);
			$prop = &$curr_tree['properties'];
			$prop['id'] = $record->uniquecity;
			$prop['species'] = $record->genus . ' ' .$record->species;
			$prop['data_source'] = 'treegenes';
			$prop['markertype'] = $record->marker_type;
			$prop['pub_tgdr'] = $record->accession;
			$prop['phenotype_city'] = $record->phenotype_city;
			$prop['pato'] = $record->pato_city;
			$prop['po'] = $record->po_city;
			$prop['genus'] = $record->genus;
			$prop['pub_year'] = $record->pyear;
			$prop['plant_group'] = $record->subkingdom;
			$prop['family'] = $record->family;
			$curr_tree['geometry']['coordinates'] = array($record->longitude, $record->latitude);
			$curr_tree['id'] = $record->geometry;
			$trees['features'][] = $curr_tree; //push to trees array
		}*/
	}
	drupal_json_output($map_records);
}


?>